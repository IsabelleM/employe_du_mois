DROP DATABASE IF EXISTS Employees;

CREATE DATABASE IF NOT EXISTS Employees;

USE Employees;

CREATE TABLE employee (
  employee_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(56),
  department enum('RD','Management','Production','Support')
);

CREATE TABLE `day` (
  day_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  dayjour date,
  employee_id int,
  shift_start_time TIME,
  shift_end_time TIME,
  break_duration TIME,
  overtime_hours TIME,
  usine_name enum('Usine A', 'Usine B', 'Usine C', 'Usine D'),
  FOREIGN KEY (employee_id) REFERENCES employee (employee_id)
);


