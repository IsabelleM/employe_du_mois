import pandas as pd
import openpyxl
import mysql.connector
from datetime import datetime
from pandas import to_timedelta, to_numeric, DataFrame


filename = 'employee.xlsx'
wb = openpyxl.load_workbook(filename)
dates = wb.sheetnames
print(dates)

df1 = pd.read_excel(filename, sheet_name=dates[1],skiprows=1)
df1.head()

# Initialiser une liste pour stocker les DataFrames de chaque feuille
dfs = {}

# Lire les autres feuilles et concaténer les DataFrames
for i, date in enumerate(dates[1:]):
    if date == '28-01-2024':
        to_del = i
        continue
    dfs[date] = pd.read_excel(filename, sheet_name=date, skiprows=1)
del dates[to_del+1]


# Concaténer tous les DataFrames dans la liste dfs en un seul DataFrame
df_concatenated = pd.concat(dfs, keys=dates[1:])
df_concatenated.reset_index(inplace=True)

print(df_concatenated)
connection = mysql.connector.connect(
    host="localhost",           
    user="david",               
    password="coucou123",       
    database="Employees"
)

# Créer un curseur pour exécuter des requêtes SQL
cursor = connection.cursor(buffered=True)

filename = 'employee.xlsx'
df_employees = pd.read_excel(filename, sheet_name='employees')
df_employees = df_employees.drop(["Unnamed: 3", "Nos employés adorés !"], axis=1)
df_employees.head()

# Insérer les données du DataFrame dans la table MySQL
for index, row in df_employees.iterrows():
    query = """INSERT INTO employee (employee_id, name, department)
    VALUES (%s, %s, %s)"""
    
    values = (row['employee_id'], row['employee_name'], row['department'])
    cursor.execute(query, values)
    employee_id = cursor.lastrowid
    connection.commit()

def trailing_zero(i):
    if i < 10:
        return True
    return False

def m_to_hm(i):
    hour = i//60
    minute = i%60
    return ":".join(
        (
            ("0" if trailing_zero(hour) else "") + str(hour),
            ("0" if trailing_zero(minute) else "") + str(minute)
        )
    )

def h_to_hm(i):
    return ("0" if trailing_zero(i) else "") + str(i) + ":00"

query = """
        INSERT INTO day (dayjour, employee_id, shift_start_time, shift_end_time, break_duration, overtime_hours, usine_name)
        VALUES (%(level_0)s, %(id)s, %(shift_start_time)s, %(shift_end_time)s, %(break_duration)s, %(overtime_hours)s, %(location)s)
        """
for res in df_concatenated.to_dict('records'):
    res['level_0'] =  datetime.strptime(res['level_0'], "%d-%m-%Y")
    res['break_duration'] = m_to_hm(res['break_duration'])
    res['overtime_hours'] = h_to_hm(res['overtime_hours'])
    print(res)
    cursor.execute(query, res)

connection.commit()
cursor.close()
connection.close()

df_concatenated = pd.to_datetime(df_concatenated['shift_start_time'], format='%H%M')
df_concatenated = pd.to_datetime(df_concatenated['shift_end_time'], format='%H%M')

df_concatenated['duration_time'] = df_concatenated['shift_end_time'] - df_concatenated['shift_end_time']
print(df_concatenated['duration_time'])