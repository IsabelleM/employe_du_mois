import pandas as pd
import openpyxl
import mysql.connector
from datetime import datetime
from pandas import to_timedelta, to_numeric, DataFrame
import numpy as np

filename = 'employee.xlsx'
wb = openpyxl.load_workbook(filename)
dates = wb.sheetnames
print(dates)

df_employees = pd.read_excel(filename, sheet_name=dates[0])
print(df_employees.head())

print(df_employees.columns)
df_employees = df_employees.drop(["Unnamed: 3", "Nos employés adorés !"], axis=1)
df_employees.head()

df1 = pd.read_excel(filename, sheet_name=dates[1],skiprows=1)
df1["date"]=dates[1]
df1.head()

# Initialiser une liste pour stocker les DataFrames de chaque feuille
dfs = []

# Lire la deuxième feuille et stocker le DataFrame dans la liste dfs
df1 = pd.read_excel(filename, sheet_name=dates[1], skiprows=1)
df1["date"] = dates[1]  # Ajouter une colonne pour la date
dfs.append(df1)


# Lire les autres feuilles et concaténer les DataFrames
for date in dates[2:]:
    if date == '28-01-2024':
        continue
    df = pd.read_excel(filename, sheet_name=date, skiprows=1)
    df["date"] = date  # Ajouter une colonne pour la date
    dfs.append(df)
print(len(dfs))


# Concaténer tous les DataFrames dans la liste dfs en un seul DataFrame
df_concatenated = pd.concat((dfs), axis=1)
print(df_concatenated.shape)
# df_concatenated = pd.to_datetime(df_concatenated['shift_start_time'], format='%H%M')
# df_concatenated = pd.to_datetime(df_concatenated['shift_end_time'], format='%H%M')



# Afficher les premières lignes du DataFrame concaténé
print(df_concatenated.head())

print(df_concatenated['date'])

connection = mysql.connector.connect(
    host="localhost",           
    user="david",               
    password="coucou123",       
    database="Employees"
)

# Créer un curseur pour exécuter des requêtes SQL
cursor = connection.cursor(buffered=True)

filename = 'employee.xlsx'
df_employees = pd.read_excel(filename, sheet_name='employees')
df_employees = df_employees.drop(["Unnamed: 3", "Nos employés adorés !"], axis=1)
df_employees.head()

# Insérer les données du DataFrame dans la table MySQL
for index, row in df_employees.iterrows():
    query = """INSERT INTO employee (employee_id, name, department)
    VALUES (%s, %s, %s)"""
    
    values = (row['employee_id'], row['employee_name'], row['department'])
    cursor.execute(query, values)
    employee_id = cursor.lastrowid
    connection.commit()

# Insérer les données du DataFrame concatenated dans la table MySQL "usine" et "day"
for index, row in df_concatenated.iterrows():
    row = list(row)


    query = """
        INSERT INTO day (dayjour, employee_id, shift_start_time, shift_end_time, break_duration, overtime_hours, usine_name)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
        """

    result = row[:-1]
    number_of_records = len(result)// 7
    for i in range(number_of_records):
        #r = dict(zip(['id', 'shift_start_time', 'shift_end_time', 'break_duration', 'overtime_hours', 'location'],result[i*7:i*7+7]))
        r = result[i*7:i*7+7]
        hour = r[3] // 60
        if hour < 10:
            hour = "0" + str(hour)
        else : hour = str(hour)
        minute = r[4] % 60

        if minute < 10:
            minute = "0" + str(minute)
        else : minute = str(minute)
        r[3] = ":".join((hour, minute)) 
        
        hour = (r[4]) // 60
        if hour < 10:
            hour = "0" + str(hour)
        else : hour = str(hour)
        minute = (r[4]) % 60

        if minute < 10:
            minute = "0" + str(minute)
        else : minute = str(minute)
        r[4] = ":".join((hour, minute))
        
        if isinstance(r[0], float):
            continue
        values = (datetime.strptime(r[6], "%d-%m-%Y"), r[0], r[1], r[2], r[3], r[4], r[5])
        
        cursor.execute(query, values)

connection.commit()
cursor.close()
connection.close()