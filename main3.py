import pandas as pd
import openpyxl
import mysql.connector
from datetime import datetime
from pandas import to_timedelta, to_numeric, DataFrame


filename = 'employee.xlsx'
wb = openpyxl.load_workbook(filename)
dates = wb.sheetnames
print(dates)

df1 = pd.read_excel(filename, sheet_name=dates[1],skiprows=1)
df1.head()

# Initialiser une liste pour stocker les DataFrames de chaque feuille
dfs = {}

# Lire les autres feuilles et concaténer les DataFrames
for i, date in enumerate(dates[1:]):
    if date == '28-01-2024':
        to_del = i
        continue
    dfs[date] = pd.read_excel(filename, sheet_name=date, skiprows=1)
del dates[to_del+1]


# Concaténer tous les DataFrames dans la liste dfs en un seul DataFrame
df_concatenated = pd.concat(dfs, keys=dates[1:])
df_concatenated.reset_index(inplace=True)



filename = 'employee.xlsx'
df_employees = pd.read_excel(filename, sheet_name='employees')
df_employees = df_employees.drop(["Unnamed: 3", "Nos employés adorés !"], axis=1)
df_employees.head()


# def trailing_zero(i):
#     if i < 10:
#         return True
#     return False

# def m_to_hm(i):
#     hour = i//60
#     minute = i%60
#     return ":".join(
#         (
#             ("0" if trailing_zero(hour) else "") + str(hour),
#             ("0" if trailing_zero(minute) else "") + str(minute)
#         )
#     )

# def h_to_hm(i):
#     return ("0" if trailing_zero(i) else "") + str(i) + ":00"

new_df_merged = pd.merge(df_employees, df_concatenated, left_on="employee_id", right_on="id")


new_df_merged = pd.merge(df_employees, df_concatenated, left_on="employee_id", right_on="id")
new_df_merged["shift_start_time"]= new_df_merged['shift_start_time'].str.split(':').apply(lambda x: int(x[0]) * 60 + int(x[1]))
new_df_merged["shift_end_time"]= new_df_merged['shift_end_time'].str.split(':').apply(lambda x: int(x[0]) * 60 + int(x[1]))
new_df_merged['duration_time']= new_df_merged['shift_end_time'] - new_df_merged['shift_start_time']
print(new_df_merged)
print(new_df_merged.columns)

new_df_merged = new_df_merged.rename(columns={'level_0': 'Date'})
print(new_df_merged.columns)

new_df_merged = new_df_merged.drop(['level_1'], axis=1)
print(new_df_merged)
print(new_df_merged.columns)

new_df_merged['difftime'] = new_df_merged['duration_time'] - new_df_merged['overtime_hours']
print(new_df_merged)
print(new_df_merged.columns)

new_df_merged = new_df_merged["employee_id"].value_counts()
print(new_df_merged)

def get_salary(new_df_merged):
    if "department" == "Production":
        result = (new_df_merged["working_time"]*12) + (new_df_merged["overtime_hours"]*125/100)
        if "location" == "Usine A":
            result = result + 15
        elif "location" == "Usine B":
            result = result + 10
        else:
            result = result + 20
    elif "department"  == "Management":
        result = (new_df_merged["working_hours"]*50) + (new_df_merged["overtime_hours"]*125/100)
        if "location" == "Usine A":
            result = result + 15
        elif "location" == "Usine B":
            result = result + 10
        else:
            result = result + 20
    elif "department" == "Support":
        result = (new_df_merged["working_hours"]*20) + (new_df_merged["overtime_hours"]*125/100)
        if "location" == "Usine A":
            result = result + 15
        elif "location" == "Usine B":
            result = result + 10
        else:
            result = result + 20
    elif "department" == "RD":
        if "location" == "Usine A":
            result = result + 15
        elif "location" == "Usine B":
            result = result + 10
        else:
            result = result + 20
    return result

print(get_salary(new_df_merged))